#include "uwiki_parse.h"

struct search {
	char* names[N_RES];
	char* urls[N_RES];
	int n;
};

void print_node(struct json_object* obj){
	printf("%s\n", json_object_to_json_string(obj));
}

search_t parse_search(char* query){
	struct json_object* root;

	struct json_object* names;
	struct json_object* urls;

	struct json_object* loop_obj;
	const char* loop_str;

	search_t ret = malloc(sizeof(*ret));

	root = json_tokener_parse(query);

	names = json_object_array_get_idx(root, 1);
	urls = json_object_array_get_idx(root, 3);

	int len = json_object_array_length(names);
	ret->n = len;

	for(int i=0; i < len; i++){
		loop_obj = json_object_array_get_idx(names, i);
		loop_str = json_object_get_string(loop_obj);

		ret->names[i] = strdup(loop_str);

		loop_obj = json_object_array_get_idx(urls, i);
		loop_str = json_object_get_string(loop_obj);

		ret->urls[i] = strdup(loop_str);
	}

	return ret; 
}

int n_result(search_t parsed){
	return parsed->n; 
}

char* get_name(search_t parsed, int i){
	return parsed->names[i];
}

char* get_url(search_t parsed, int i){
	return parsed->urls[i];
}

void search_destroy(search_t search){
	for(int i=0; i < search->n; i++){
		free(search->names[i]);
		free(search->urls[i]);
	}

	free(search);
}
