/* defaults */
static const char* wiki_url = "https://it.wikipedia.org/w/api.php";
static const char* browser = "surf";

typedef struct {
	char* code;
	char* url;
} wiki;

static const wiki wikis[] = {
	{ "it", "https://it.wikipedia.org/w/api.php" },
	{ "en", "https://en.wikipedia.org/w/api.php" },
	{ "pms", "https://pms.wikipedia.org/w/api.php" },
	{ "arch", "https://wiki.archlinux.org/api.php"  },
};
