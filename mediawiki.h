#include <curl/curl.h>
#include <stdlib.h>
#include <string.h>

typedef struct mediawiki_struct* mediawiki_t;

mediawiki_t mediawiki_init();
char* search_results(mediawiki_t, char**, int);
void mediawiki_destroy(mediawiki_t);
