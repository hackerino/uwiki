/* requires client to include config.h and uwiki_parse.h */

#include <stdio.h>
#include "uwiki_parse.h"
#include "config.h"

#define LENGTH(X)	sizeof(X)/sizeof(X[0])

void usage(char* exec);
int select_result(search_t parsed);
void list_wikis();
void set_wiki();
char* get_wiki_by_code(char* code);
char* uwiki_config();
char* get_system_browser();
