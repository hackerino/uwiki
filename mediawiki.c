#include "mediawiki.h"

#define API_PARAM "?action=opensearch&format=json&search="
#define URL_LENGTH 512
#define DEFAULT_BUFFER 64

struct mediawiki_struct {
	char* buffer;
	int size;
	char* url;
};

size_t append_buffer(char* ptr, size_t size, size_t nmemb, void* userdata){
	mediawiki_t mw = userdata;

	static int written = 0;
	static int buffer_size = DEFAULT_BUFFER;

	int realsize = size * nmemb;

	while(written + realsize >= buffer_size){
		buffer_size *= 2;
		mw->buffer = realloc(mw->buffer, buffer_size);
		mw->size = buffer_size;
	}

	strcat(mw->buffer, ptr);

	written += realsize;
	mw->buffer[written] = '\0';

	return realsize;
}

mediawiki_t mediawiki_init(char* wiki)
{
	mediawiki_t mw = malloc(sizeof(*mw));

	mw->buffer = malloc(DEFAULT_BUFFER);
	strcpy(mw->buffer, "");

	mw->url = strdup(wiki);

	return mw;
}

char* search_url(mediawiki_t mw, char** terms, int n)
{
	if(mw->url == NULL){
		printf("mw->url not initialized\n");
		return NULL;
	}

	mw->url = realloc(mw->url, URL_LENGTH);
	strcat(mw->url, API_PARAM);

	for(int i=0; i<n; i++){
		strcat(mw->url, terms[i]);
		strcat(mw->url, "+");
	}

	return mw->url;
}

char* search_results(mediawiki_t mw, char** terms, int n)
{	
	CURL* curl = curl_easy_init();

	char* query_url = search_url(mw, terms, n);

	curl_easy_setopt(curl, CURLOPT_WRITEDATA, mw);
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, append_buffer);
	curl_easy_setopt(curl, CURLOPT_URL, query_url);

	curl_easy_perform(curl);
	curl_easy_cleanup(curl);

	free(query_url);

	return mw->buffer;
}

void mediawiki_destroy(mediawiki_t mw){
	free(mw->buffer);
	free(mw);
}
