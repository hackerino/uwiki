#include "uwiki_util.h"

int select_result(search_t parsed){
	int i, n;

	n = n_result(parsed);

	if( n == 0){
		printf("No results\n");
		return -1;
	}

	if(n != 1){
		for(i=0; i < n; i++)
			printf("[%d] %s\n", i, get_name(parsed, i));

		printf("Open link number: ");
		scanf("%d", &i);

	}else
		i = 0;

	return i;
}

void list_wikis(){
	int i = 0;

	for(i=0; i < LENGTH(wikis); i++)
		printf("%s\t%s\n", wikis[i].code, wikis[i].url);
}

char* get_wiki_by_code(char* code){
	for(int i=0; i < LENGTH(wikis); i++){
		if(strcmp(wikis[i].code, code) == 0)
			return wikis[i].url;
	}

	return NULL;
}

char* uwiki_config(char* code, int select){
	char* ret = wiki_url;

	if(code)
		ret = get_wiki_by_code(code);

	if(!ret){
		printf("Inserted code does not exist, using default wiki\n");
	}else
		printf("Selected %s\n", ret);

	return ret;
}

void dump_results(search_t parsed){
	int n = n_result(parsed);

	for(int i=0; i<n; i++)
		printf("%s %s\n", get_name(parsed, i), get_url(parsed, i));
}

void usage(char* exec){
	printf("USAGE: %s [-hdl] [-w code] [-b browser]\n", exec);
}
