#include <json-c/json.h>
#include <stdio.h>
#include <string.h>

#define N_RES 10

typedef struct search* search_t;

search_t parse_search(char*);
int n_result(search_t);
char* get_name(search_t, int);
char* get_url(search_t, int);
void search_destroy(search_t);
