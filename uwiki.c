#include "mediawiki.h"
#include "uwiki_util.h"
#include <unistd.h>

#include <errno.h>

int main(int argc, char** argv){

	mediawiki_t mw;
	search_t parsed;
	char command[100], *wiki_code = NULL, *results, *url;
	int i, opt, execute = 1, dump = 0;

	/* options parsing */
	while((opt = getopt(argc, argv, "b:hldw:")) != -1){
		switch(opt){
			case 'b':
				browser = optarg;
				break;
			case 'd':
				dump = 1;
				break;
			case 'h':
				usage(argv[0]);
				return 0;
			case 'w':
				wiki_code = strdup(optarg);
				break;
			case 'l':
				list_wikis();
				return 0;
		}
	}


	/* init */
	url = uwiki_config(wiki_code, 1);
	free(wiki_code);

	/* selected wiki code is not available */
	if(url == NULL)
		execute = 0;

	mw = mediawiki_init(url);

	if(argc-optind == 0){
		printf("No keywords, exiting...\n");
		return -1;
	}

	/* searching and displaying */
	results = search_results(mw, argv+optind, argc-optind);
	parsed = parse_search(results);

	if(!dump)
		i = select_result(parsed);
	else{
		dump_results(parsed);
		return 0;
	}

	/* no result available to select from */
	if(i == -1)
		execute = 0;

	if(execute){
		/* opening browser */
		sprintf(command, "%s '%s'", browser, get_url(parsed, i));
		printf("Executing %s\n", command);
		system(command);
	}

	/* cleanup */
	search_destroy(parsed);
	mediawiki_destroy(mw);

	return 0;
}
