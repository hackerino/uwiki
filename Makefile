CC = gcc
CFLAGS = -g -Wall
INCLUDES = $(shell pkg-config --cflags json-c)
LDFLAGS = $(shell pkg-config --libs json-c libcurl)

all: options uwiki

options:
	@echo "CFLAGS   = ${CFLAGS}"
	@echo "LDFLAGS  = ${LDFLAGS}"
	@echo "CC       = ${CC}"

uwiki: uwiki.o uwiki_util.o uwiki_parse.o mediawiki.o
	$(CC) $(CFLAGS) $(INCLUDES) -o uwiki uwiki.o uwiki_parse.o uwiki_util.o mediawiki.o $(LDFLAGS) 

uwiki.o: uwiki.c
	$(CC) $(CFLAGS) -c uwiki.c 

uwiki_util.o: uwiki_util.h uwiki_util.c
	$(CC)  $(CFLAGS) -c uwiki_util.c

uwiki_parse.o: uwiki_parse.h uwiki_parse.c
	$(CC) $(CFLAGS)  -c uwiki_parse.c

mediawiki.o: mediawiki.h mediawiki.c
	$(CC) $(CFLAGS)  -c mediawiki.c

clean:
	rm -rf *.o; rm uwiki

install: all
	cp uwiki /usr/bin
