# uwiki
uwiki is a simple tool to search on every Mediawiki-powered wiki using the OpenSearch API, written in C using libcurl.

## Configuration
All the necessary configuration is done in the `config.h` file, like you do for the suckless' software.

In the default `config.h` file is present an example to get you started.

## Compiling
The only dependency required is libcurl. Other than that, it is a simple matter of:

* git clone https://gitlab.com/hackerino/uwiki.git
* cd uwiki
* make
* sudo make install

## Usage
The program is used like the following:
```
uwiki [options] keywords
```

Where the options are:
* `-w code`	to select the wiki identified by the given code in the `wikis` array (in config.h)
* `-l`		to list the available codes and wikis (basically dumps the wikis array)
* `-b browser`	to select an alternative browser (needs to be in the $PATH)	

For both `-w` and `-b`, if they aren't defined, the program will default to the values `wiki_url` and `browser`, defined in `config.h`. 
